#!/usr/bin/perl
use strict;
use Getopt::Long qw/GetOptions :config no_auto_abbrev/;
use Pod::Usage;
use File::Basename 'dirname';
use File::Spec;
use Coro;
use lib join '/', File::Spec->splitdir(dirname(__FILE__)), 'lib';

use Parser;
use Spider;
use DB;
use Util qw/parse_date_str/;

my %opts;
my ($help, $start_date, $end_date, $e2e, $e2e_interval, $suffix, $processes);
my ($min, $max, $between, $doc_type);
my $log = sub {};
my $r = GetOptions(
    'help|h'     => \$help,
    'verbose!'   => sub { $log = sub { printf shift() ."\n", @_; } },
    'between!'   => \$between,
    'min!'       => \$min,
    'max!'       => \$max,
    'e2e'        => \$e2e,
    'tor-host'   => sub {},
    'skip-tor'   => sub {},
    'process:i'    => \$processes,
    'start-date:s' => sub { $start_date = parse_date_str($_[1]) },
    'end-date:s'   => sub { $end_date = parse_date_str($_[1]) },
    'doc-type:s'   => \$doc_type,
    'e2e-interval' => \$e2e_interval,
    'version'    => \$suffix,
);
$processes ||= 1;
$e2e_interval ||= 1;

#
# some global objects
#
my $db = new DB({
        dbname => 'journ_test',
        user => 'root',
        suffix => $suffix,
    });
my $lock = new Coro::Semaphore $processes;
my $now = DateTime->now();

my $max_scraped_date = $db->max_scraped_date();
my $min_scraped_date = $db->min_scraped_date() || DateTime->now;

#
# main selector
#
if($min and $end_date) {
    run_batch($min_scraped_date, $end_date, 0, 1);
}
elsif($max) {
    my $week_ago = $now->clone->subtract( days => 7 );
    run_batch($max_scraped_date, $week_ago, 0, 0);
}
elsif($e2e) {
    my $today_e2e = $now->clone->subtract( days => $e2e_interval);
    if($today_e2e >= $max_scraped_date) {
        run_batch($max_scraped_date, $max_scraped_date->clone->add(days => $e2e_interval), 1, 0);
    }
    else {
        run_batch($today_e2e, $now, 1, 0);
    }
}
elsif($start_date and $end_date) {
    run_batch($start_date, $end_date, $e2e, $between);
}
else {

die <<"EOF" if $help;
usage: $0 [OPTIONS] [APPLICATION]

  > $0 --start-date 10/10/1895 --end-date 11/10/1895
  > $0 --end-date 01/01/1995 --min --doc-type ANSOL,AOC,DEEDS

These options are available:
  -h, --help                     Show this message.
  --start-date <date>            Start date
  --end-date <date>              End date
  --between                      Start date is greater than end date - scraping backdated
  --min                          Backdated scrape between min_scraped_date and end_date
  --max                          Forward scrape between max_scraped_date and today()-7
  --tor-host                     Default localhost (preferable tor host - if not provided the random one will be used)
  --skip-tor                     default 0
  --doc-type                     document type, comma separated string.
                                 Scrape only specified document type(s) if specified
  --e2e                          Scrape with updating documents
  --process                      default 1 - by the request
  --verbose                      Print details about what files changed to
                                 STDOUT.

EOF

}

#
# useful functions
#

sub run_batch {
    my ($start_date, $end_date, $e2e, $backwards) = @_;
    if(!$db->tables_exist()) {
        $log->("no tables found, creating..");
        $db->create_tables();
        $log->("done");
    }
    $log->("starting scrape from date:%s", $start_date->dmy);
    my $sp = new Spider({
            date => $start_date,
            doc_type => $doc_type,
        });
    while(my $content = $sp->next_page) {
        # fetch details in parallel and then return all data as single result
        my @data = parse_page_data($sp, $content);
        unless(scalar @data) {
            my $next_date = $backwards ? $start_date->subtract(days => 1) : $start_date->add(days => 1);
            if($backwards ? $next_date >= $end_date : $next_date <= $end_date) {
                $log->("no more results here, stepping to next date: %s", $next_date->dmy);
                $sp->set_date($next_date);
                next;
            }
            else {
                last;
            }
        }
        my @data = fetch_details_async( $sp, check_row_existence( @data ) );
        if($e2e) { 
            my $updated = 0;
            for(@data) {
                $updated += $db->add_data_row($_, 1);
            }
            $log->("updated %s rows of %s results", $updated, scalar @data);
        }
        else {
            my $added = 0;
            for(@data) {
                $added += $db->add_data_row($_);
            }
            $log->("inserted %s rows of %s", $added, scalar @data);
        }
    }

    $log->("scrape process ended, thank you");
}

sub parse_page_data {
    my ($sp, $content) = @_;
    my @data = Parser::parse_obj_table($content);
    if(scalar @data == 0) {
        # data is over
        return;
    }
    my ($range_from, $range_to, $ntotal) = Parser::parse_ntotal($content);
    $log->("fetched page %s (%s-%s of %s results)", $sp->page, $range_from, $range_to, $ntotal);
    return @data;
}

sub check_row_existence {
    my (@data) = @_;
    map {
        $_->{obj_exists} = $db->get_row_by_fileno($_->{file_no}) ? 1 : 0;
        $_;
    } @data;
}

sub fetch_details_async {
    my ($sp, @data) = @_;
    my @dt = map {
        my $o = $_;
        async {
            my $g = $lock->guard;
            if(!$o->{obj_exists} or $e2e) {
                #$log->("fetching %s", $o->{details_url});
                eval {
                    my $html = $sp->fetch_url($o->{details_url});
                    $o->{details} = Parser::parse_details($html);
                };
                if($@) {
                    $log->("Error fetching details: %s", $@); 
                }
            }
            $o;
        }
    } @data;
    map { $_->join } @dt;
}

__END__

=head1 NAME

journ_test.pl - Test script

=head1 SYNOPSIS

  $ journ_test.pl --start-date 10/10/1895 --end-date 11/10/1895

=cut

