use Test::More;
use strict;
use lib 'lib';

use Parser;

my $html = `cat t/test_page.html`;
my $det_html = `cat t/details_test_page.html`;
my $det_html1 = `cat t/details_test_page1.html`;

my @data = Parser::parse_obj_table($html);

is_deeply( Parser::parse_details($det_html), {
        info => 'PART REL UNIT-1 SAILSIDE CONDO',
        remarks => '2-34 30.00 31.00',
        direct => ['M & T MORTGAGE CORPORATION', 'MANUFACTURERS & TRADERS TRUST COMPANY'],
        indirect => ['MILLER & SMITH HOMES AT PENINSULA LLC'],
        town => 'Not Applicable',
        consideration => '',
        type_desc => 'MISCELLANEOUS'
    });
is_deeply( Parser::parse_details($det_html1), {
        info => 'CCH L-A-14',
        remarks => '',
        direct => ['MASON  ERVIN T', 'MASON  LADDA N'],
        indirect => ['WACHOVIA BANK OF DELAWARE NATIONAL ASSN'],
        town => 'Not Applicable',
        consideration => '',
        type_desc => 'MORTGAGE'
    });
is_deeply( [Parser::parse_ntotal($html)], [226,250,211286] ) ;
is( scalar @data, 25 );
is_deeply( $data[0], {
        collateral => '2519', type_desc => 'LIEN',
        posted_date => '2012-01-24', inst_date => '01/25/2012',
        npgs => 7, file_no => 'M/12697/234',
        details_url => 'controller?commandflag=getDetails&optflag=DetailsCommand&county=de005&userid=null&userCategory=7&ptrno=1986663&instrumentnumber=2519&fileDate=01/24/2012 12:49:05&instDate=01/25/2012&book=M/&volume=12697/&page=234&doctype=All&partytype=&officeid=60&fromdate=11/07/1999&todate=01/25/2012'
    } );

done_testing();
