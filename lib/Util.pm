package Util;
use strict;
use DateTime;

require Exporter;
our @ISA = ('Exporter');
our @EXPORT = qw(
    parse_date_str
);

sub parse_date_str {
    ($_) = @_;
    /(\d{4})[-\/.](\d\d)[-\/.](\d\d)/ and return DateTime->new(year => $1, month => $3, day => $2);
    /(\d\d)[-\/.](\d\d)[-\/.](\d{4})/ and return DateTime->new(year => $3, month => $1, day => $2);
    return;
}

1;
