package Parser;
use strict;
use v5.10;
use HTML::TreeBuilder;

sub parse_ntotal {
    my ($html) = @_;
    my @r = $html =~ /Result\sMatches.+\[(\d+)[ -]+(\d+)\s*of\s*(\d+)\]/s;
    @r;
}


sub parse_obj_table {
    my ($html) = @_;
    #warn $html;
    my $tree = HTML::TreeBuilder->new;
    $tree->parse($html);
    $tree->eof();
    my @data;
    my @cols = qw/collateral posted_date type_desc inst_date file_no npgs/;

    for my $tr ($tree->look_down('_tag','tr','id', qr/^myRow\d+/)) {
        my @row = 
            map { $_->as_trimmed_text }
            map { $_->look_down('_tag','font')  }
            $tr->look_down('_tag','td');
        my %row = map { $cols[$_] => $row[$_] } 0..scalar @cols - 1;
        $row{inst_date} =~ /\// or $row{inst_date} = '';
        $row{posted_date} = $row{posted_date} =~ m|(\d\d)/(\d\d)/(\d{4})| ? "$3-$1-$2" : undef;
        $row{details_url} = eval { 
            my @a = $tr->look_down('_tag','a');
            $a[1]->attr('href');
        };
        push @data, \%row;
    }

    @data;
}

sub parse_details {
    my ($html) = @_;
    my $data = {};
    ($data->{remarks}) = $html =~ m|Remarks.+?<font[^>]+>([^<]*)</font|s;
    ($data->{info}) = $html =~ m|\sInformation.+?<font[^>]+>([^<]*)</font|s;
    #my ($top_tbl) = $html =~ m|Comments</font.+?<TR[^>]+>(.+?)</TR>|sgi;
    my ($top_tbl) = $html =~ m|src="/delr/images/searchbottom\.gif".+?<TR\sbgcolor="#ffffff">(.+?)</TR>|sgi;
    my ($indirect) = $html =~ m|<B>Indirect.+?<table>(.+?)</table>|s;
    my ($direct) = $html =~ m|<B>Direct.+?<table>(.+?)</table>|s;
    @{ $data->{direct} } = $direct =~ m|<font[^>]+>([^<]+)</font>|sgi;
    @{ $data->{indirect} } = $indirect =~ m|<font[^>]+>([^<]+)</font>|sgi;
    my @top_tbl = $top_tbl =~ m|<font[^>]+>([^<]*)</font>|sgi;

    $data->{type_desc} = $top_tbl[2];
    $data->{town} = $top_tbl[6];
    $data->{consideration} = $top_tbl[7];

    
    for my $k (keys %$data) {
        $data->{$k} = ref $data->{$k} ? [ map { trim($_) } @{ $data->{$k} } ] : trim($data->{$k});
    }
    $data;
}

sub trim { $_ = shift; s/&[a-z0-9]+;//gi; s/^[\r\n\s]+//; s/[\r\n\s]+$//; $_ }

1;
