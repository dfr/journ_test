package Spider;
use Coro::LWP;
use Coro::AnyEvent;
use WWW::Mechanize;
require LWP::UserAgent;
use Parser qw/parse_obj_table parse_ntotal/;


use strict;
use constant AGENT => 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30';
use constant START_URL => 'https://de.uslandrecords.com/delr/DelrApp/index.jsp';

sub new {
    my ($cls, $params) = @_;
    my $mech = WWW::Mechanize->new(stack_depth => 5, agent => AGENT);
    my $ua = LWP::UserAgent->new(agent => AGENT);
    my @doc_types = split /,/, $params->{doc_type};
    bless {
        doc_types => scalar @doc_types ? \@doc_types : ['All'],
        mech => $mech,
        ua => $ua,
        date => $params->{date},
    }, $cls;
}

sub set_date {
    my ($self,$date) = @_;
    $self->{date} = $date;
    undef $self->{curr_page};
}

sub next_page {
    my ($self) = @_;
    #my $t = async { 
    if(! $self->{curr_page}) {
        $self->_first_page();
    }
    else {
        $self->_next_page();
    }
    #};
    #$t->join;
    $self->{mech}->content;
}

sub _first_page {
    my ($self) = @_;
    
    $self->{curr_page} = 1;
    $self->{mech}->get(START_URL);
    $self->{mech}->submit_form(
        form_name => 'commonForm',
        fields    => {
            optflag     => 'MenuCommand',
            commandflag => 'index.jsp',
            countycode  => 'de005',
            countypage  => 'true'
        },
    );

    $self->{mech}->form_name('searchform');
    $self->{mech}->set_fields(
            fromdate => $self->{date}->strftime('%d/%m/%Y'),
            todate => $self->{date}->strftime('%d/%m/%Y'),
            rowincrement => 25,
            isSubscriptionActive => 'false',
            commandflag => 'searchByInstrument',
            searchType => 'searchByInstrument',
            optflag => 'SearchCommand',
            instfrom => '1',
            instto => '9999999',
            volume => '2',
            ucctype => 'All',
    );
    $self->{mech}->select('doctype', $self->{doc_types});
    $self->{mech}->submit;
}

sub _next_page {
    my ($self) = @_;
    eval { $self->{mech}->follow_link( text => 'Next' ) };
    if($@) {
        warn $self->{mech}->content;
        warn $@;
        return;
    }
    else {
        $self->{curr_page} += 1;
    }
}

sub page {
    my ($self) = @_;
    $self->{curr_page};
}

sub fetch_url {
    my ($self, $url) = @_;
    my $m = $self->{mech}->clone;
    $m->follow_link(url => $url);
    $m->content;
}

1;

