package DB;
use strict;
use DBI;
use DateTime;

my $DOC_TABLE= 'recorder_document';
my $DIR_TABLE = 'recorder_direct';

=pod
Helper storage-related functions
=cut

sub new {
    my ($cls, $params) = @_;
    my $port = $params->{port} || '3306';
    my $host = $params->{host};
    my $dsn = $host
        ? sprintf "DBI:mysql:database=%s;host=%s;port=%s", $params->{dbname}, $host, $port
        : sprintf "DBI:mysql:database=%s", $params->{dbname};

    my $dbh = DBI->connect($dsn,
        $params->{user}, $params->{password},
        {RaiseError => 1, AutoCommit => 0});

    bless {dbh => $dbh, suffix => $params->{suffix}}, $cls;
}

sub get_row_by_fileno {
    my ($self, $fileno) = @_;
    my $doc_tbl = $DOC_TABLE . $self->{suffix};
    my $result;
    $self->txn_do(sub {
        my $r = $self->{dbh}->selectcol_arrayref("SELECT id FROM $doc_tbl WHERE doc_number = ?", {}, $fileno);
        if(scalar @$r > 0) {
            $result = $r->[0];
        }
    });
    return $result;
}

sub add_data_row {
    my ($self, $row, $update) = @_;
    my $doc_tbl = $DOC_TABLE . $self->{suffix};
    my $dir_tbl = $DIR_TABLE . $self->{suffix};
    $self->txn_do(sub {
        my $r = $self->{dbh}->selectcol_arrayref("SELECT id FROM $doc_tbl WHERE doc_number = ?", {}, $row->{file_no});
        if(scalar @$r > 0 && !$update) {
            return
        }
        else {
            $self->{dbh}->do("DELETE FROM $doc_tbl WHERE doc_number = ?", {}, $row->{file_no});
        }
        my $sth1 = $self->{dbh}->prepare("INSERT INTO $doc_tbl (exported_at, doc_number, info, remarks, town, consideration, type_desc,  date) VALUES (NOW(),?,?,?,?,?,?,?)");
        my $sth2 = $self->{dbh}->prepare("INSERT INTO $dir_tbl (fk_doc_number, title, type) VALUES (?,?,?)");
        
        $sth1->execute($row->{file_no}, $row->{details}->{info},
            $row->{details}->{remarks},
            $row->{details}->{town},
            $row->{details}->{consideration},
            $row->{details}->{type_desc},
            $row->{posted_date} );
        for my $det (@{ $row->{details}->{direct} }) {
            $sth2->execute($row->{file_no}, $det, 'DIR' );
        }
        for my $det (@{ $row->{details}->{indirect} }) {
            $sth2->execute($row->{file_no}, $det, 'INDIR' );
        }
    }); # txn_do
}

sub min_scraped_date {
    my ($self) = @_;
    $self->_scraped_date('ASC');
}

sub max_scraped_date {
    my ($self) = @_;
    $self->_scraped_date('DESC');
}

sub _scraped_date {
    my ($self, $sort) = @_;
    my $doc_tbl = $DOC_TABLE . $self->{suffix};
    my $r = [];
    if($self->tables_exist) {
        $self->txn_do(sub {
            $r = $self->{dbh}->selectcol_arrayref("SELECT UNIX_TIMESTAMP(date) FROM $doc_tbl ORDER BY DATE $sort LIMIT 1");
        });
    }
    if(scalar @$r > 0) {
        DateTime->from_epoch( epoch => $r->[0] );
    }
    else {
        DateTime->now
    }
}

sub drop_tables {
    my ($self) = @_;
    my $suffix = $self->{suffix};
    $self->txn_do(sub { 
        for($DIR_TABLE, $DOC_TABLE) {
            $self->{dbh}->do(qq/DROP TABLE IF EXISTS $_$suffix/);
        }
    });
}

sub tables_exist {
    my ($self) = @_;
    my $suffix = $self->{suffix};
    my $sql = qq/SHOW TABLES LIKE '$DOC_TABLE$suffix'/;
    my $r;
    $self->txn_do(sub { $r = $self->{dbh}->selectcol_arrayref($sql); });
    scalar @$r > 0;
}

sub create_tables {
    my ($self) = @_;
    my $suffix = $self->{suffix};
    my @sql = (qq/
    CREATE TABLE $DOC_TABLE$suffix (
    id int(11) auto_increment,
    doc_number varchar(20) not null default '',
    info TEXT,
    remarks TEXT,
    town VARCHAR(255),
    consideration VARCHAR(255),
    type_desc VARCHAR(255),
    date date,
    updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    exported_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
    PRIMARY KEY (id),
    UNIQUE KEY (doc_number),
    KEY (date),
    KEY (updated_at),
    KEY (exported_at)
    ) ENGINE=InnoDB;/,
    qq/CREATE TABLE $DIR_TABLE$suffix (
    id int(11) auto_increment,
    title TEXT,
    type enum('DIR', 'INDIR') NOT NULL DEFAULT 'DIR',
    fk_doc_number varchar(20) not null default '',
    PRIMARY KEY (id),
    FOREIGN KEY (fk_doc_number) REFERENCES $DOC_TABLE$suffix(doc_number) ON DELETE CASCADE
    ) ENGINE=InnoDB;
    /);
    $self->txn_do(sub { $self->{dbh}->do($_) for @sql; });
}

sub recreate_tables {
    my ($self, $s) = @_;
    $self->drop_tables($s) and $self->create_tables($s);
}

sub txn_do {
    my ($self, $block) = @_;

    eval {
        my $r = $block->();
        $self->{dbh}->commit;   # commit the changes if we get this far
    };
    if ($@) {
        warn $@;
        eval { $self->{dbh}->rollback };
        0;
    }
    else {
        1;
    }
}

1;
